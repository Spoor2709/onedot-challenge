# Dictionary Management app

This project has been made to assist OneDot customers manage dictionaries, used to
clean or normalise values in dataset columns.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

npm

### Installing and developing

To Install this app locally run

```sh
$ npm install
```

```sh
$ npm start
```

then open your browser to http://localhost:3000/<br />

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Advanced build and configuration

```sh
$ npm run eject
```

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### Production build

```sh
$ npm run build
```

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

## Built With

- [React](https://reactjs.org/) - The web framework used
- [Redux](https://redux.js.org/) - Global State Management used
- [Material UI](https://material-ui.com/) - React component library used
- [uuid](https://www.npmjs.com/package/uuid) - unique id generator

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

- **James Spoor** - _Initial work_ - [Spoor2709](https://github.com/Spoor2709)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
