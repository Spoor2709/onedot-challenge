import React from "react";
import Routes from "./Containers/Routes";
import { BrowserRouter as Router, Switch } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Routes/>
      </Switch>
    </Router>
  );
}

export default App;
