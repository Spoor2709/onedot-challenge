import { combineReducers } from "redux";
import { dictionariesReducer } from "./dictionariesReducer";

export const reducers = combineReducers({
  dictionariesReducer
});
