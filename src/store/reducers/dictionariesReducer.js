import {
  ADD_DICTIONARY,
  DELETE_DICTIONARY,
  UPDATE_DICTIONARY_ENTRIES
} from "../types";

const initialState = [
  {
    id: 1,
    name: "Colors",
    category: "Descriptive",
    description: "This is a prefilled Dictionary, following the example given",
    entries: [
      { domain: "Stonegrey", range: "Dark Grey" },
      { domain: "Midnight Black", range: "Back" },
      { domain: "Mystic Silver", range: "Silver" },
      { domain: "Stonegrey", range: "Dark Grey" },
      { domain: "Silver", range: "Mystic Silver" }
    ]
  },
  {
    id: 2,
    name: "Countries",
    category: "Descriptive",
    description:
      "This is a Dictionary filled with countries and thier country codes",
    entries: [
      { domain: "India", range: "IN" },
      { domain: "China", range: "CN" },
      { domain: "Italy", range: "IT" },
      { domain: "United States", range: "US" },
      { domain: "IN", range: "India" },
      { domain: "China", range: "CN" }
    ]
  }
];

export const dictionariesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DICTIONARY: {
      let newState = [...state, action.payload];
      return newState;
    }
    case DELETE_DICTIONARY: {
      let newState = state.filter(
        dictionary => dictionary.id !== action.payload
      );
      return newState;
    }
    case UPDATE_DICTIONARY_ENTRIES: {
      let newState = state.map(dictionary =>
        dictionary.id === action.payload.id ? action.payload : dictionary
      );
      return newState;
    }

    default:
      return state;
  }
};
