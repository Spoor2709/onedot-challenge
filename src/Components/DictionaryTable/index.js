import React from "react";
import { useDispatch } from "react-redux";
import MaterialTable from "material-table";
import { UPDATE_DICTIONARY_ENTRIES } from "../../store/types";

const DictionaryTable = props => {
  const dispatch = useDispatch();
  const dictionary = props.dictionary;

  const [state, setState] = React.useState({
    columns: [
      { title: "Domain", field: "domain" },
      { title: "Range", field: "range" }
    ],
    data: dictionary.entries
  });

  const updateDictionary = newState => {
    const newDictionary = { ...dictionary, entries: newState.data };
    dispatch({ type: UPDATE_DICTIONARY_ENTRIES, payload: newDictionary });
  };

  return (
    <>
      {dictionary ? (
        <MaterialTable
          title={dictionary.name}
          columns={state.columns}
          data={state.data}
          options={{
            exportButton: true
          }}
          editable={{
            onRowAdd: newData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...state.data];
                  data.push(newData);
                  const newState = { ...state, data };
                  setState(newState);
                  updateDictionary(newState);
                }, 600);
              }),
            onRowUpdate: (newData, oldData) =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  if (oldData) {
                    const data = [...state.data];
                    data[data.indexOf(oldData)] = newData;
                    const newState = { ...state, data };
                    setState(newState);
                    updateDictionary(newState);
                  }
                }, 600);
              }),
            onRowDelete: oldData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...state.data];
                  data.splice(data.indexOf(oldData), 1);
                  const newState = { ...state, data };
                  setState(newState);
                  updateDictionary(newState);
                }, 600);
              })
          }}
        />
      ) : (
        Error
      )}
    </>
  );
};

export default DictionaryTable;
