import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  card: {
    minWidth: 275
  },
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  pos: {
    marginBottom: 12
  }
}));

const DictionaryDisplayCard = props => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} gutterBottom>
          {props.title}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {props.category}
        </Typography>
        <Typography variant="body2" component="p">
          {props.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Link to={`/detail/${props.id}`}>
          <Button size="small">View Dictionary</Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default DictionaryDisplayCard;
