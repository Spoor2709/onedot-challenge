import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  UPDATE_DICTIONARY_ENTRIES,
  DELETE_DICTIONARY
} from "../../store/types";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./styles";

const EditDictionaryDetails = props => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const [buttonType, setButtonType] = useState("Edit Dictionary Details");

  const [values, setValues] = useState({
    name: props.dictionary.name,
    category: props.dictionary.category,
    description: props.dictionary.description
  });
  const handleClick = e => {
    e.preventDefault();
    if (buttonType === "Edit Dictionary Details") {
      setButtonType("Save");
    } else {
      saveDetails();
      setButtonType("Edit Dictionary Details");
    }
  };
  const saveDetails = () => {
    const updatedDictionary = {
      ...props.dictionary,
      name: values.name,
      category: values.category,
      description: values.description
    };
    dispatch({ type: UPDATE_DICTIONARY_ENTRIES, payload: updatedDictionary });
  };
  const handleChange = e => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const handleDelete = () => {
    dispatch({ type: DELETE_DICTIONARY, payload: props.dictionary.id });
    props.history.push("/");
  };

  return (
    <Card className={classes.card}>
      <CardContent>
        {buttonType !== "Save" ? (
          <>
            <Typography className={classes.title} gutterBottom>
              {values.name}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {values.category}
            </Typography>
            <Typography variant="body2" component="p">
              {values.description}
            </Typography>
          </>
        ) : (
          <>
            <div class={classes.topContainer}>
              <TextField
                name="name"
                id="outlined-error-helper-text"
                label="Name"
                value={values.name}
                variant="outlined"
                onChange={handleChange}
              />
              <TextField
                name="category"
                id="outlined-error-helper-text"
                label="Category"
                variant="outlined"
                value={values.category}
                onChange={handleChange}
              />
            </div>
            <TextField
              name="description"
              label="Description"
              multiline
              rows="2"
              variant="outlined"
              value={values.description}
              fullWidth
              onChange={handleChange}
            />
          </>
        )}
      </CardContent>
      <CardActions>
        <Button size="small" onClick={handleClick}>
          {buttonType}
        </Button>
        <Button size="small" onClick={handleDelete}>
          Delete Dictionary
        </Button>
      </CardActions>
    </Card>
  );
};

export default withRouter(EditDictionaryDetails);
