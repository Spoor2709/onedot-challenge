import { makeStyles } from "@material-ui/core/styles";
export const useStyles = makeStyles(theme => ({
  card: {
    minWidth: 275
  },
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  pos: {
    marginBottom: 12
  },
  topContainer: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "5vh"
  }
}));
