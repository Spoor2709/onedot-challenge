import React, { useEffect, useState } from "react";
import { useStyles } from "./styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const DictionaryWarnings = ({ dictionary }) => {
  const classes = useStyles();
  const [isErrors, setIsErrors] = useState(false);
  const [errors, setErrors] = useState({
    dupliactes: [],
    forks: [],
    cycles: [],
    chains: []
  });
  useEffect(() => {
    console.log("called");
    if (dictionary.entries.length > 2) {
      setIsErrors(false);
      const clearErrors = { ...errors };
      clearErrors.dupliactes = [];
      clearErrors.forks = [];
      clearErrors.cycles = [];
      clearErrors.chains = [];
      setErrors(clearErrors);
    } else {
      checkEntryErrors();
    }
  }, [dictionary.entries]);

  useEffect(() => {
    if (!isErrors) {
      checkEntryErrors();
    }
  }, [isErrors]);

  const checkEntryErrors = () => {
    let errorFound = false;
    for (let i = 0; i < dictionary.entries.length; i++) {
      for (let j = i + 1; j < dictionary.entries.length; j++) {
        if (dictionary.entries[i].domain === dictionary.entries[j].domain) {
          // duplicates or forks
          if (dictionary.entries[i].range === dictionary.entries[j].range) {
            // duplicates
            const newErrors = { ...errors };
            newErrors.dupliactes.push({ i, j });
            setErrors(newErrors);
            setIsErrors(true);
          } else {
            // fork
            const newErrors = { ...errors };
            newErrors.forks.push({ i, j });
            setErrors(newErrors);
            setIsErrors(true);
          }
        } else if (
          dictionary.entries[i].domain === dictionary.entries[j].range
        ) {
          // cycles or chains
          if (dictionary.entries[i].range === dictionary.entries[j].domain) {
            // cycle
            const newErrors = { ...errors };
            newErrors.cycles.push({ i, j });
            setErrors(newErrors);
            setIsErrors(true);
          } else {
            //chain
            const newErrors = { ...errors };
            newErrors.chains.push({ i, j });
            setErrors(newErrors);
            setIsErrors(true);
          }
        }
      }
    }
  };

  return (
    <>
      {isErrors ? (
        <Card className={classes.card}>
          <CardContent>
            <Typography className={classes.title} gutterBottom>
              Dictionary Errors Found
            </Typography>
            {errors && errors.dupliactes[0]
              ? errors.dupliactes.map(duplicate => (
                  <Typography variant="body2" component="p" color="primary">
                    {`Duplicates found on line ${duplicate.i +
                      1} and on line ${duplicate.j + 1}`}
                  </Typography>
                ))
              : null}
            {errors && errors.forks[0]
              ? errors.forks.map(fork => (
                  <Typography variant="body2" component="p" color="primary">
                    {`Fork found on line ${fork.i + 1} and on line ${fork.j +
                      1}`}
                  </Typography>
                ))
              : null}
            {errors && errors.cycles[0]
              ? errors.cycles.map(cycle => (
                  <Typography variant="body2" component="p" color="secondary">
                    {`Cycle found on line ${cycle.i + 1} and on line ${cycle.j +
                      1}`}
                  </Typography>
                ))
              : null}
            {errors && errors.chains[0]
              ? errors.chains.map(chain => (
                  <Typography variant="body2" component="p" color="secondary">
                    {`Chain found on line ${chain.i + 1} and on line ${chain.j +
                      1}`}
                  </Typography>
                ))
              : null}
          </CardContent>
          <CardActions></CardActions>
        </Card>
      ) : null}
    </>
  );
};

export default DictionaryWarnings;
