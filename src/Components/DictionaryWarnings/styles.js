import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  card: {
    minWidth: 275,
    marginTop: "5vh"
  },
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  pos: {
    marginBottom: 12
  }
}));
