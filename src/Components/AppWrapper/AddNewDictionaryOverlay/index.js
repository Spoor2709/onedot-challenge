import React, { useState, useEffect } from "react";
import { useStyles } from "./styles";
import { Grid, Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import uuid from "uuid";
import { useDispatch } from "react-redux";
import { ADD_DICTIONARY } from "../../../store/types";
import CloseIcon from "@material-ui/icons/Close";

export const AddNewDictionaryOverlay = props => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [values, setValues] = useState({
    name: "",
    category: "",
    description: ""
  });

  const [valid, setValid] = useState({
    name: false,
    category: false,
    description: false
  });

  const handleChange = e => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    setValid({
      name: values.name.length > 0 ? true : false,
      category: values.category.length > 0 ? true : false,
      description: values.description.length > 0 ? true : false
    });
  }, [values]);

  const handleSubmit = async () => {
    if (Object.values(valid).every(v => v)) {
      const newDictionary = {
        id: uuid(),
        name: values.name,
        category: values.category,
        description: values.description,
        entries: []
      };
      const addedDictionary = dispatch({
        type: ADD_DICTIONARY,
        payload: newDictionary
      });
      closeOverlay();
    }
  };

  const closeOverlay = () => {
    setValues({
      name: "",
      category: "",
      description: ""
    });
    props.toggle();
  };

  return (
    <>
      {props.active ? (
        <div className={classes.root}>
          <Card className={classes.card}>
            <CardContent>
              <div className={classes.titleContainer}>
                <Typography className={classes.title} gutterBottom>
                  Add New Dictionary
                </Typography>
                <Button size="small" onClick={closeOverlay}>
                  <CloseIcon />
                </Button>
              </div>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <TextField
                    error={!valid.name}
                    name="name"
                    fullWidth
                    id="outlined-error-helper-text"
                    label="Name"
                    value={values.name}
                    variant="outlined"
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    error={!valid.category}
                    name="category"
                    fullWidth
                    id="outlined-error-helper-text"
                    label="Category"
                    variant="outlined"
                    value={values.category}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={!valid.description}
                    name="description"
                    label="Description"
                    multiline
                    rows="2"
                    variant="outlined"
                    value={values.description}
                    fullWidth
                    onChange={handleChange}
                  />
                </Grid>
              </Grid>
            </CardContent>
            <CardActions className={classes.buttonContainer}>
              <Button size="small" onClick={handleSubmit}>
                Submit
              </Button>
            </CardActions>
          </Card>
        </div>
      ) : null}
    </>
  );
};
