import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    position: "sticky",
    marginBottom: "-92.2vh",
    background: "rgba(0, 0, 0, 0.8)",
    zIndex: 10,
    top: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "92.2vh",
    width: "100%"
  },
  card: {
    position: "inherit",
    width: "50%",
    height: "auto",
    padding: theme.spacing(4)
  },
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  titleContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: theme.spacing(3),
    marginTop: -theme.spacing(2)
  },
  buttonContainer: {
    display: "flex",
    justifyContent: "flex-end",
    padding: theme.spacing(2)
  }
}));
