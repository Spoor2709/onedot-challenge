import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Header from "./Header";
import SideBar from "./SideBar";
import { useStyles } from "./styles";
import { AddNewDictionaryOverlay } from "./AddNewDictionaryOverlay";

const AppWrapper = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [overlay, setOverlay] = React.useState(false);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const addDictionaryOverlayToggle = () => {
    setOverlay(!overlay);
  };

  return (
    <>
      <div className={classes.root}>
        <CssBaseline />
        <Header handleDrawerOpen={handleDrawerOpen} open={open} />
        <SideBar
          handleDrawerClose={handleDrawerClose}
          open={open}
          dictionaryToggle={addDictionaryOverlayToggle}
        />
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <AddNewDictionaryOverlay
            active={overlay}
            toggle={addDictionaryOverlayToggle}
          />
          {props.children}
        </main>
      </div>
    </>
  );
};

export default AppWrapper;
