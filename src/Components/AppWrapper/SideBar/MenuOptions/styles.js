import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  link: {
    textDecoration: "inherit",
    color: "inherit"
  }
}));
