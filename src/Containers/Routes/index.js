import React from "react";
import { Route, withRouter } from "react-router-dom";
import AppWrapper from "../../Components/AppWrapper";
import Home from "../Pages/HomePage";
import DictionaryDetailPage from "../Pages/DictionaryDetailPage";
import InformationPage from "../Pages/InformationPage";

const Routes = () => {
  return (
    <>
      <AppWrapper>
        <Route exact path="/" component={Home} />
        <Route exact path="/detail/:id" component={DictionaryDetailPage} />
        <Route exact path="/info" component={InformationPage} />
      </AppWrapper>
    </>
  );
};

export default withRouter(Routes);
