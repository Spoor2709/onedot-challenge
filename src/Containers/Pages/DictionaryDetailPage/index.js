import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { useStyles } from "./styles";
import DictionaryTable from "../../../Components/DictionaryTable";
import EditDictionaryDetails from "../../../Components/EditDictionaryDetails";
import DictionaryWarnings from "../../../Components/DictionaryWarnings";
import { useSelector } from "react-redux";
import { withRouter } from "react-router-dom";

const DictionaryDetailPage = props => {
  const classes = useStyles();

  const dictionary = useSelector(({ dictionariesReducer }) => {
    return dictionariesReducer.find(
      dictionary => dictionary.id == props.match.params.id
    );
  });

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Grid container spacing={3}>
        <Grid item lg={7} md={12} xs={12}>
          <DictionaryTable dictionary={dictionary} />
        </Grid>
        <Grid item lg={5} md={12} xs={12}>
          <EditDictionaryDetails dictionary={dictionary} />

          <DictionaryWarnings dictionary={dictionary} />
        </Grid>
      </Grid>
    </Container>
  );
};

export default withRouter(DictionaryDetailPage);
