import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { useSelector } from "react-redux";
import { useStyles } from "./styles";
import DictionaryDisplayCard from "../../../Components/DictionaryDisplayCard";

const Home = () => {
  const dictionaryList = useSelector(
    ({ dictionariesReducer }) => dictionariesReducer
  );
  const classes = useStyles();
  return (
    <Container maxWidth="lg" className={classes.container}>
      <Grid container spacing={3}>
        {dictionaryList.map(dictionary => {
          return (
            <Grid key={dictionary.id} item xs={12} sm={6} lg={4}>
              <DictionaryDisplayCard
                id={dictionary.id}
                title={dictionary.name}
                category={dictionary.category}
                description={dictionary.description}
              />
            </Grid>
          );
        })}
      </Grid>
    </Container>
  );
};

export default Home;
