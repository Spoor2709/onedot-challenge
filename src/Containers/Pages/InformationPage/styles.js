import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  card: {
    minWidth: 275
  },
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  pos: {
    marginBottom: 12
  }
}));
