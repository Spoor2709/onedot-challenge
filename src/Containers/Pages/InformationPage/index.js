import React from "react";
import { useStyles } from "./styles";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";

const InformationPage = () => {
  const classes = useStyles();
  return (
    <Container maxWidth="lg" className={classes.container}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8}>
          <Card className={classes.card}>
            <CardContent>
              <Typography className={classes.title} gutterBottom>
                Information
              </Typography>
              <Typography variant="body1" component="p">
                The dictionary management app was created to assist OneDot
                customers to manage thier product detail dictionaries.
                <ul>
                  <li>
                    To use this app create a new dictionary, be sure to fill out
                    all of the required details.
                  </li>
                  <li>
                    Upon creating a new dicitionary You can then view your new
                    empty dictionary
                  </li>
                  <li>
                    You can then create multiple domain range value pairs.
                  </li>
                  <li>
                    Our software will flag up if you have made any errors in
                    creating your dictionary.
                  </li>
                </ul>
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={4}>
          <Card className={classes.card}>
            <CardContent>
              <Typography className={classes.title} gutterBottom>
                Contact
              </Typography>
              <Typography variant="body1" component="p">
                If you have any issues using this app, please contact us on
                <ul>
                  <li>
                    <a href="+41777777781">+41777777781</a>
                  </li>
                  <li>
                    <a href="email@onedot.com">email@onedot.com</a>
                  </li>
                </ul>
                Happy Sorting
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default InformationPage;
